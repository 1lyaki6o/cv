#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN_STR 102
#define MAX_LEN_LINE 115

#define max(x, y) ((x) < (y) ? (y) : (x))

typedef struct Node {
	char* name;
	long long ip;
	struct Node* left;
	struct Node* right;
	int height;

} Node;

long long tree_search(Node* root, char* name) {
	if (root == NULL) {
		return -1;
	} else if (strcmp(name, root->name) == 0) {
		return root->ip;
	} else if (strcmp(name, root->name) < 0) {
		return tree_search(root->left, name);
	} else if (strcmp(name, root->name) > 0) {
		return tree_search(root->right, name);
	}

	return 0;
}

int height(Node* root) {
	return root ? root->height : 0;
}

Node* delete_tree(Node* root) {
	if (root != NULL) {
		delete_tree (root->right);
		delete_tree(root->left);
		free(root->name);
		free(root);
	}

	return NULL;
}

Node* rotate_L(Node* root) {
	Node* new = root->left;
	root->left = new->right;
	new->right = root;

	root->height = max(height(root->left), height(root->right)) + 1;
	new->height = max(height(new->left), root->height) + 1;

	return new;
}

Node* rotate_R(Node* root) {
	Node* new = root->right;
	root->right = new->left;
	new->left = root;

	root->height = max(height(root->left), height(root->right)) + 1;
	new->height = max(height(new->right), root->height) + 1;

	return new;
}

Node* rotate_LR(Node* root) { // double L
	root->left = rotate_R(root->left);

	return rotate_L(root);
}

Node* rotate_RL(Node* root) { // double R
	root->right = rotate_L(root->right);

	return rotate_R(root);
}

Node* insert (char* name, long long ip, Node* root) {
	if (root == NULL) {
		root = (Node*)malloc(sizeof(Node));

		if (!root) {
			abort();
		}

		root->ip = ip;
		root->name = name;
		root->height = 1;
		root->left = NULL;
		root->right = NULL;
	} else if (strcmp(name, root->name) < 0) {
		root->left = insert(name, ip, root->left);

		if (height(root->left) - height(root->right) == 2) {
			if (strcmp(name, root->left->name) < 0) {
				root = rotate_L(root);
			} else {
				root = rotate_LR(root);
			}
		}
	} else if (strcmp(name, root->name) > 0) {
		root->right = insert(name, ip, root->right);

		if (height(root->right) - height(root->left) == 2) {
			if (strcmp(name, root->right->name) > 0) {
				root = rotate_R(root);
			} else {
				root = rotate_RL(root);
			}
		}
	} else {
		root->height = max(height(root->left), height(root->right)) + 1;
	}

	return root;
}


int main(void) {
	char input[] = "input.txt";
	char output[] = "output.txt";

	FILE* fp = fopen(input, "r");
	FILE* fh = fopen(output, "w");

	if (!fp) {
		printf("Cannot open file '%s'\n", input);
	}

	if (!fh) {
		printf("Cannot open file '%s'\n", output);
	}

	int n;

	if (fscanf(fp, "%d\n", &n) != 1) {
		printf("Cannot read int from file '%s'\n", input);
	}

	Node* root = NULL;
	char buf_line[MAX_LEN_LINE];

	for (int i = 0; i < n; i++) {
		char* name = (char*)malloc(MAX_LEN_STR * sizeof(char));

		fgets(buf_line, MAX_LEN_LINE, fp);

		int j = 0;

		while (!(buf_line[j] >= '0' && buf_line[j] <= '9') && buf_line[j] != ' ') {
			name[j] = buf_line[j];
			j++;
		}

		name[j] = '\0';

		while (buf_line[j] == ' ') {
			j++;
		}

		long long ip = 0;

		while (buf_line[j] >= '0' && buf_line[j] <= '9') {
			ip *= 10;
			ip += buf_line[j++] - '0';
		}

		root = insert(name, ip, root);
	}

	int m;

	if (fscanf(fp, "%d\n", &m) != 1) {
		printf("Cannot read int from file '%s'\n", input);
	}

	for (int i = 0; i < m; i++) {
		fgets(buf_line, MAX_LEN_LINE, fp);

		int str_len = strlen(buf_line) - 1;
		buf_line[str_len] = '\0';


		fprintf(fh, "%lld\n", tree_search(root, buf_line));
	}

	fclose(fp);
	fclose(fh);

	delete_tree(root);

	return 0;
}

