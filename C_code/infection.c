#include <stdio.h>
#include <stdlib.h>

#define MAX_INFECTION_POINTS 4

typedef struct point {
	int x;
	int y;
} point;

int infection (int N, int M, point mas_virus[], int virus_cnt, int all_virus, int time, int matrix[][M]) {
	if (all_virus == N * M) {
		return time;
	}

	point new[virus_cnt * MAX_INFECTION_POINTS];
	int inf_points = 0;

	for (int i = 0; i < virus_cnt; i++) {
		for (int j = 1; j < 9; j += 2) {
			int tmp_1 = mas_virus[i].x - 1 + j / 3;
			int tmp_2 = mas_virus[i].y - 1 + j % 3;

			if ((tmp_1 >= 0 && tmp_1 < N) &&
			        (tmp_2 >= 0 && tmp_2 < M) && matrix[tmp_1][tmp_2] == 0) {
				matrix[tmp_1][tmp_2] = 1;
				new[inf_points].x = tmp_1;
				new[inf_points].y = tmp_2;
				inf_points++;
			}
		}
	}

	time++;

	return infection(N, M, new, inf_points, all_virus + inf_points, time, matrix);
}

int main(void) {
	int N, M;
	scanf("%d%d", &N, &M);

	int matrix[N][M];

	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++) {
			matrix[i][j] = 0;
		}
	}

	int virus_cnt;
	scanf("%d", &virus_cnt);

	point mas_virus[virus_cnt];
	int a, b;

	for (int i = 0; i < virus_cnt; i++) {
		scanf("%d", &a);
		scanf("%d", &b);
		mas_virus[i].x = b - 1;
		mas_virus[i].y = a - 1;
	}

	for (int i = 0; i < virus_cnt; i++) {
		matrix[mas_virus[i].x][mas_virus[i].y] = 1;
	}

	int time = 0, all_virus = virus_cnt;

	time = infection(N, M, mas_virus, virus_cnt, all_virus, time, matrix);

	printf("%d", time);

	return 0;
}
