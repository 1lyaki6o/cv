#include <stdio.h>
#include <stdlib.h>

#define MAX_STEPS_POW 100

/*Составим массив 0 и 1, где 0 - умножение матрицы на саму себя, 1 - умножение на исходную матрицу*/
void algorithm_mltu(int* mas_alg, int degree, int* p) {
	if (degree == 1) {
		return;
	}

	if (degree % 2 != 0) {
		mas_alg[(*p)++] = 1;

		return algorithm_mltu(mas_alg, degree - 1, p);
	} else {
		mas_alg[(*p)++] = 0;
		return algorithm_mltu(mas_alg, degree / 2, p);
	}
}

void fast_pow_matrix(long** const_matrix, long** matrix, int* algorithm, int cnt_mutl, int k, int mod) {
	if (cnt_mutl < 0) {
		return;
	}

	long res_mas[k][k];

	for (int i = 0; i < k; i++) {
		for (int j = 0; j < k; j++) {
			res_mas[i][j] = 0;
		}
	}

	for (int i = 0; i < k; i++) {
		for (int j = 0; j < k; j++) {
			for (int p = 0; p < k; p++) {
				if (algorithm[cnt_mutl] == 0) {
					res_mas[i][j] += (matrix[i][p] * matrix[p][j]) % mod;
				} else {
					res_mas[i][j] += (const_matrix[i][p] * matrix[p][j]) % mod;
				}
			}

			res_mas[i][j] %= mod;
		}
	}

	for (int i = 0; i < k; i++) {
		for (int j = 0; j < k; j++) {
			matrix[i][j] = res_mas[i][j];
		}
	}

	return fast_pow_matrix(const_matrix, matrix, algorithm, cnt_mutl - 1, k, mod);
}

int main(void) {
	int k, N, p;
	scanf("%d%d%d", &k, &N, &p);

	int degree = N - k;
	int mas_value[k];

	for (int i = 0; i < k; i++) {
		scanf("%d", &mas_value[i]);
	}

	long** coef_matrix = (long**)calloc(k, sizeof(long*));
	long** const_coef_matrix = (long**)calloc(k, sizeof(long*));

	for (int i = 0; i < k; i++) {
		coef_matrix[i] = (long*)calloc(k, sizeof(long));
		const_coef_matrix[i] = (long*)calloc(k, sizeof(long));
	}

	for (int i = k - 1; i >= 0; i--) {
		for (int j = k - 1; j >= 0; j--) {
			if (i - j == 1) {
				coef_matrix[i][j] = 1;
				const_coef_matrix[i][j] = 1;
			} else if (j == k - 1) {
				scanf("%ld", &coef_matrix[i][j]);
				const_coef_matrix[i][j] = coef_matrix[i][j];
			} else {
				coef_matrix[i][j] = 0;
				const_coef_matrix[i][j] = 0;
			}
		}
	}

	if (degree <= 0) {
		printf("%d", mas_value[N - 1] % p);
		return 0;
	}

	int cnt_mutl = 0;
	int* mas_alg = (int*)calloc(MAX_STEPS_POW, sizeof(int));
	algorithm_mltu(mas_alg, degree, &cnt_mutl);

	fast_pow_matrix(const_coef_matrix, coef_matrix, mas_alg, cnt_mutl - 1, k, p);

	int res = 0;

	for (int i = 0; i < k; i++) {
		res += mas_value[i] * coef_matrix[i][k - 1];
	}

	printf("%d\n", res % p);

	for (int i = 0; i < k; i++) {
		free(const_coef_matrix[i]);
		free(coef_matrix[i]);
	}

	free(const_coef_matrix);
	free(coef_matrix);
	free(mas_alg);

	return 0;
}
