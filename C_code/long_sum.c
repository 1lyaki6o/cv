#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN_STR ((int) 2e4 + 3)
#define MAX_CNT ((int) 1e4 + 2)

int main(void) {
	char line[MAX_LEN_STR];
	fgets(line, MAX_LEN_STR, stdin);

	char number_1[MAX_CNT], number_2[MAX_CNT], operation;
	int i = 0;

	while (line[i] >= '0' && line[i] <= '9') {
		number_1[i] = line[i];
		i++;
	}

	operation = line[i++];
	int k = 0;

	while (line[i] >= '0' && line[i] <= '9') {
		number_2[k] = line[i];
		i++;
		k++;
	}

	int l_1 = strlen(number_1) ;
	int l_2 = strlen(number_2) ;
	int flag = 0;

	if (l_1 > l_2) {
		flag = 1;
	} else if (l_2 > l_1) {
		flag = 0;
	} else {
		int d = strcmp(number_1, number_2);

		if (d > 0) {
			flag = 1;
		} else if (d < 0) {
			flag = 0;
		} else if (d == 0 && operation == '-') {
			printf("0");
			return 0;
		}
	}

	if (flag == 0) {
		char tmp[MAX_CNT];
		strcpy(tmp, number_1);
		strcpy(number_1, number_2);
		strcpy(number_2, tmp);
		l_1 = strlen(number_1);
		l_2 = strlen(number_2);
	}

	k = 0;
	int int_of_num = 0;
	char res[MAX_LEN_STR * 2];

	for (int i = 0; i < l_1 + l_2 * 2 - 1; i++) {
		res[i] = '0';
	}

	if (operation == '+') {
		strcpy(res, number_1);

		while (k != l_2) {
			char x = number_1[l_1 - 1 - k];
			char y = number_2[l_2 - 1 - k];
			res[l_1 - 1 - k] = (x + y - 2 * '0' + int_of_num) % 10 + '0';
			int_of_num = (x + y - 2 * '0' + int_of_num) / 10;
			k++;
		}

		while (l_1 - k > 0 && int_of_num != 0) {
			char z = number_1[l_1 - 1 - k];
			res[l_1 - 1 - k] = (z - '0' + int_of_num) % 10 + '0';
			int_of_num = (z - '0' + int_of_num) / 10;
			k++;
		}

		if (int_of_num != 0) {
			printf("%c", int_of_num + '0');
		}

		printf("%s\n", res);
	} else if (operation == '-') {
		strcpy(res, number_1);

		while (k != l_2) {
			char x = number_1[l_1 - 1 - k];
			char y = number_2[l_2 - 1 - k];
			res[l_1 - 1 - k] = (10 + x - y - int_of_num) % 10 + '0';
			int_of_num = 1 - (10 + x - y - int_of_num) / 10;
			k++;
		}

		while (l_1 - k > 0 && int_of_num != 0) {
			char z = number_1[l_1 - 1 - k];
			res[l_1 - 1 - k] = (10 + z - '0' - int_of_num) % 10 + '0';
			int_of_num = 1 - (10 + z - '0' - int_of_num) / 10;
			k++;
		}

		if (flag == 0) {
			printf("-");
		}

		k = 0;

		while (res[k] == '0') {
			k++;
		}

		for (int i = k; res[i] != '\0'; i++) {
			printf("%c", res[i]);
		}
	} else if (operation == '*') {
		int mult_shift = l_2 * 2 - 1;
		int i, save_prev_res;

		if (number_2[0] == '0') {
			printf("0");
			return 0;
		}

		while (k != l_2) {
			for (i = l_1 - 1; i >= 0; i--) {
				char y = number_2[l_2 - 1 - k];
				save_prev_res = res[i + mult_shift];
				res[i + mult_shift] = ((y - '0') * (number_1[i] - '0') +
						int_of_num + (save_prev_res - '0')) % 10 + '0';
				int_of_num = ((y - '0') * (number_1[i] - '0') +
						int_of_num + (save_prev_res - '0')) / 10;

			}

			if (int_of_num != 0) {
				res[i + mult_shift] = int_of_num + '0';
			}

			int_of_num = 0;
			k++;
			mult_shift--;
		}

		k = 0;

		while (res[k] == '0') {
			k++;
		}

		for (int i = k; i < l_1 + l_2 * 2 - 1; i++) {
			printf("%c", res[i]);
		}
	}

	return 0;
}
