#include <stdio.h>
#include <stdlib.h>

void swap(int* x, int* y) {
	int tmp = *x;
	*x = *y;
	*y = tmp;
}

int max_of_3(int x_1, int x_2, int x_3) {
	if (x_3 < x_2) {
		swap(&x_3, &x_2);
	}

	if (x_3 < x_1) {
		swap(&x_3, &x_1);
	}

	return x_3;
}

int main(void) {
	int N, M;
	scanf("%d%d", &N, &M);

	int mas[N + 2][M];

	for (int i = 0; i < N + 2; i++) {
		for (int j = 0; j < M; j++) {
			if (i == 0 || i == N + 1) {
				mas[i][j] = 0;
			} else {
				scanf("%d", &mas[i][j]);
			}
		}
	}

	for (int j = 1; j < M; j++) {
		for (int i = 1; i < N + 1; i++) {
			mas[i][j] += max_of_3(mas[i - 1][j - 1], mas[i][j - 1], mas[i + 1][j - 1]);
		}
	}

	int max_cost = 0;

	for (int i = 1; i < N + 1; i++) {
		if (mas[i][M - 1] > max_cost) {
			max_cost = mas[i][M - 1];
		}
	}

	printf("%d", max_cost);

	return 0;
}