#include <stdio.h>
#include <stdlib.h>

typedef struct Node {
	int value;
	struct Node* next;
} Node;

void delete_list (Node** head) {
	Node* cur = *head;

	while (cur) {
		Node* next = cur->next;
		free(cur);
		cur = next;
	}

	*head = NULL;
}

void printf_list(Node* head) {
	Node* cur = head;

	while (cur) {
		printf("%d ", cur->value);
		cur = cur->next;
	}
}

Node* push_front(Node* head, int value) {
	Node* new = (Node*)malloc(sizeof(Node));

	new->value = value;
	new->next = head;

	return new;
}

void split_list(Node* head, Node** a, Node** b) {
	Node* step = head;
	Node* duble_step = head->next;

	while (duble_step != NULL) {
		duble_step = duble_step->next;

		if (duble_step != NULL) {
			step = step->next;
			duble_step = duble_step->next;
		}
	}

	*b = step->next;
	step->next = NULL;
	*a = head;
}

Node* merge_list(Node* a, Node* b, Node* new_head) {
	if (a == NULL) {
		new_head = b;
	} else if (b == NULL) {
		new_head = a;
	} else {
		if (a->value <= b->value) {
			new_head = a;
			new_head->next = merge_list(a->next, b, new_head->next);
		} else {
			new_head = b;
			new_head->next = merge_list(a, b->next, new_head->next);
		}
	}

	return new_head;
}

Node* merge_sort(Node* head) {
	Node* first_half;
	Node* second_half;

	if (head == NULL || head->next == NULL) {
		return head;
	}

	split_list(head, &first_half, &second_half);

	first_half = merge_sort(first_half);
	second_half = merge_sort(second_half);

	Node* new_head = merge_list(first_half, second_half, new_head);

	return new_head;
}


int main(void) {
	char input[] = "input.txt";
	char output[] = "output.txt";

	FILE* fp = fopen(input, "r");

	if (!fp) {
		printf("Cannot open file '%s'\n", input);
	}

	Node* head = NULL;
	int numb;

	while (fscanf(fp, "%d", &numb) == 1) {
		head = push_front(head, numb);
	}

	head = merge_sort(head);

	fclose(fp);

	FILE* fh = fopen(output, "w");

	if (!fh) {
		printf("Cannot open file '%s'\n", output);
	}

	Node* cur = head;

	while (cur) {
		fprintf(fh, "%d ", cur->value);
		cur = cur->next;
	}

	fclose (fh);

	delete_list(&head);

	return 0;
}

