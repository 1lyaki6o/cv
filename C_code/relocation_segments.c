#include <stdio.h>
#include <stdlib.h>

typedef struct Node {
	int value;
	struct Node* next;
	struct Node* prev;
} Node;

Node* push_front(struct Node* head, int value) {
	struct Node* new_node = (struct Node*)malloc(sizeof(struct Node));

	new_node->value = value;
	new_node->next = head;
	new_node->prev = NULL;
	if (head != NULL) {
		head->prev = new_node;
	}

	return new_node;
}


int main(void) {
	char filename_1[] = "input.txt";
	char filename_2[] = "output.txt";

	FILE* fp = fopen(filename_1, "r");

	if (!fp) {
		printf("Cannot open file '%s\n'", filename_1);
		return 1;
	}

	int n, m;

	if (fscanf(fp, "%d%d", &n, &m) != 2) {
		printf("Cannot read from file '%s'", filename_1);
		fclose(fp);
		return 1;
	}

	Node* head = NULL;
	Node* arr_pointers[n + 1];

	for (int i = n; i > 0; i--) {
		head = push_front(head, i);
		arr_pointers[i] = head;
	}

	int x, y; //начало и конец отрезка перемещения

	for (int k = 0; k < m; k++) {
		if (fscanf(fp, "%d%d", &x, &y) != 2) {
			printf("Cannot read from file '%s'", filename_1);
		}

		if (arr_pointers[x]->prev != NULL) {
			arr_pointers[x]->prev->next = arr_pointers[y]->next;

			if (arr_pointers[y]->next != NULL) {
				arr_pointers[y]->next->prev = arr_pointers[x]->prev;
			}

			arr_pointers[y]->next = head;
			head->prev = arr_pointers[y];
			arr_pointers[x]->prev = NULL;
			head = arr_pointers[x];
		}
	}

	fclose(fp);

	FILE* fh = fopen(filename_2, "w");

	if (!fh) {
		printf("Cannot open file '%s\n'", filename_1);
		return 1;
	}

	Node* cur = head;

	while (cur) {
		fprintf(fh, "%d ", cur->value);
		cur = cur->next;
	}

	fclose(fh);

	while (head->next != NULL) {
		head = head->next;
		free(head->prev);
	}

	free(head);

	return 0;
}
