#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_BUF ((int) 1e4)
#define	MAX_STR_LEN ((int) 102)
#define	MAX_LINE_LEN ((int) 2e3 + 2)

typedef struct line {
	char* line;
	int number;
	char* str;
} line;

void swap(line* p_1, line* p_2) {
	line tmp = *p_1;
	*p_1 = *p_2;
	*p_2 = tmp;
}

int compare_int(const void* p1, const void* p2) {
	const line* n1 = p1;
	const line* n2 = p2;

	return n1->number - n2->number;
}

int compare_str(const void* p1, const void* p2) {
	const line* s1 = p1;
	const line* s2 = p2;

	return strcmp(s1->str, s2->str);
}

void qsort_struct(line* arr, int left, int right, int (*compare)(const void *, const void *)) {
	int i = left;
	int j = right;

	line mid = arr[(left + right) / 2];

	do {
		while (compare(&arr[i], &mid) < 0 && i < right) {
			i++;
		}

		while (compare(&arr[j], &mid) > 0 && j > left) {
			j--;
		}

		if (i <= j) {
			swap(&arr[i], &arr[j]);

			i++;
			j--;
		}
	} while (i <= j);


	if (left < j) {
		qsort_struct(arr, left, j, compare);
	}

	if (i < right) {
		qsort_struct(arr, i, right, compare);
	}
}

int main(void) {
	char filename_1[] = "input.txt";
	char filename_2[] = "output.txt";

	FILE* fp = fopen(filename_1, "r");

	if (!fp) {
		printf("Cannot open file '%s\n'", filename_1);
		return 1;
	}

	int n;

	if (fscanf(fp, "%d\n", &n) != 1) {
		printf("Cannot read int from '%s'\n", filename_1);
	}

	line* arr_lines = (line*)malloc(MAX_BUF * sizeof(line));

	for (int i = 0; i < MAX_BUF; i++) {
		arr_lines[i].line = (char*)malloc(MAX_LINE_LEN * sizeof(char));
		arr_lines[i].str = (char*)malloc(MAX_STR_LEN * sizeof(char));
	}

	char buf_line[MAX_LINE_LEN], field_str[MAX_STR_LEN];

	int line_counter = 0, elem_line_cnt = 0, elem_str_cnt = 0;
	int flag_sort = 0, flag_space = 0;
	int find_field = 0; //Счетчик для поиска необходимого поля

	while (fgets(buf_line, MAX_LINE_LEN, fp)) {
		for (int i = 0; buf_line[i] != '\0'; i++) {
			if (buf_line[i] != ' ') {
				arr_lines[line_counter].line[elem_line_cnt++] = buf_line[i]; // создаем строку, которую будем вывожить

				if (buf_line[i] == ';') { // ищем нужное поле
					find_field++;
				} else if (find_field == n) { // попали в нужное поле
					if (buf_line[i] == '"') { // определяем, что будем сравнивать (строки или числа)
						flag_sort = 1;
					}

					field_str[elem_str_cnt++] = buf_line[i]; // набираем универсальную строку в нужном поле
				}

				/*Дальше флаг поднимается,если мы первый раз встретили ковычку,
				и опускается, когда когда мы снова встречаем ковычку. Таким образом
				определяется момент, когда пробелы из исходной строки buf_line нужно сохранить*/
				if (buf_line[i] == '"' && flag_space == 0) {
					flag_space = 1;
				} else if (buf_line[i] == '"' && flag_space == 1) {
					flag_space = 0;
				}
			} else if (buf_line[i] == ' ' && flag_space == 1) { // с помощью flag_spece записываются пробелы
				arr_lines[line_counter].line[elem_line_cnt++] = buf_line[i];
			}
		}

		field_str[elem_str_cnt] = '\0';

		if (flag_sort == 0) { // с помощью flag_sort определяем, что находится в нужнос поле
			arr_lines[line_counter].number = atoi(field_str);
		} else {
			strcpy(arr_lines[line_counter].str, field_str);
		}

		line_counter++;
		elem_line_cnt = 0;
		elem_str_cnt = 0;
		find_field = 0;
	}

	if (flag_sort == 0) {
		qsort_struct(arr_lines, 0, line_counter - 1, compare_int);
	} else {
		qsort_struct(arr_lines, 0, line_counter - 1, compare_str);
	}

	fclose(fp);

	FILE* fh = fopen(filename_2, "w");

	if (!fh) {
		printf("Cannot open filr '%s\n'", filename_1);
		return 1;
	}

	for (int i = 0; i < line_counter; i++) {
		fprintf(fh, "%s", arr_lines[i].line);
	}

	fclose(fh);

	for (int i = 0; i < MAX_BUF; i++) {
		free(arr_lines[i].line);
		free(arr_lines[i].str);
	}

	free(arr_lines);

	return 0;
}