#include <stdio.h>
#include <math.h>

int is_prime(int x) {
	for (int i = 2; i <= (int) sqrt(x); i++) {
		if (x % i == 0) {
			return 0;
		}
	}

	return 1;
}

void hyper_prime(int x, int N) {
	int mas[] = {1, 3, 7, 9};
	int degree_of_ten = 1, i = 0;

	while (x != 0) {
		x = x * 10 + mas[i];
		degree_of_ten++;

		while (is_prime(x)) {
			if (degree_of_ten == N) {
				printf("%d ", x);
				break;
			} else {
				degree_of_ten++;
				i = 0;
				x = x * 10 + mas[i];
			}
		}

		while (i == 3) {
			x /= 10;
			degree_of_ten--;
			i = (x % 10) / 3;
		}

		x /= 10;
		degree_of_ten--;
		i++;
	}

}

int main(void) {
	int N;
	scanf("%d", &N);

	if (N == 1) {
		printf("2 3 5 7");
	} else {
		hyper_prime(2, N);
		hyper_prime(3, N);
		hyper_prime(5, N);
		hyper_prime(7, N);
	}

	return 0;
}