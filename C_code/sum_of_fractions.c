#include <stdio.h>

int gcd(int a, int b) {
	return a % b != 0 ? gcd(b, a % b) : b;
}

//X - целая часть дроби; Y - числитель дроби; Z - знаменатель дроби

int sum_fractoin(int X, int Y, int Z, int cnt_remaining_frac) {
	if (cnt_remaining_frac == 0) {
		if (Y != 0) {
			int simple = gcd(Z, Y);
			printf("%d %d %d", X, Y / simple, Z / simple);
		} else {
			printf("%d %d %d", X, Y, Z);
		}

		return 0;
	}

	int a, b;
	scanf("%d%d", &a, &b);

	if (Z != b) {
		Y *= b;
		a *= Z;
		Z *= b;
	}

	if (Y + a < Z) {
		Y += a;
	} else {
		X += (Y + a) / Z;
		Y = (Y + a) % Z;

		if (Y == 0) {
			Z = 1;
		}
	}

	return sum_fractoin(X, Y, Z, cnt_remaining_frac - 1);
}

int main(void) {
	int N;
	scanf("%d", &N);

	int a, b; // первое число
	scanf("%d%d", &a, &b);

	sum_fractoin(0, a, b, N - 1);

	return 0;
}