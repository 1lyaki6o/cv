%include "io.inc"

section .bss
    n resd 1
    k resd 1
    sum resd 1
    prev resd 1
    
section .data

   
section .text
global CMAIN
CMAIN:
    push ebp
    mov ebp, esp
    GET_UDEC 4, n
    GET_UDEC 4, k
    
    push dword[k]
    push dword[n]
    
    call sum_on_board
    
    add esp, 8
    
    add dword[sum], eax
    PRINT_UDEC 4, [sum]
    
    mov esp, ebp
    pop ebp
    
    xor eax, eax
    ret
    
    
sum_on_board:
    push ebp
    mov ebp, esp
    sub esp, 4
    mov eax, [ebp + 8]
    cmp eax, [prev]
    jne .recur
    jmp .end
  .recur:
    add dword[sum], eax
    mov dword[prev], eax
    
    push dword[k]
    push eax
    
    call sum_of_numbers
    
    add esp, 8
    
    mov dword[esp], eax
    call sum_on_board
  .end: 
    mov esp, ebp
    pop ebp
    
    ret
    
sum_of_numbers:
    push ebp
    mov ebp, esp
    
    mov eax, [ebp + 8]
    mov ecx, [ebp + 12]
    xor ebx, ebx
    
  .while:
    cmp eax, 0
    jbe .end
    xor edx, edx
    div ecx
    add ebx, edx
    jmp .while
  .end:  
    mov eax, ebx
    mov esp, ebp
    pop ebp
    
    ret
