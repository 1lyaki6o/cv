%include "io.inc"

extern fscanf
extern fprintf
extern qsort
extern fopen
extern fclose 

INT_SIZE equ 4

section .bss
    arr resd 1000
    x resd 1
    fp resd 1
section .rodata
    filename_1 db `input.txt`, 0
    open_mode_1 db `r`, 0
    filename_2 db `output.txt`, 0
    open_mode_2 db `w`, 0
    f_print db `%d `, 0
    f_scanf db `%d`, 0
   
section .text
GLOBAL CMAIN
CMAIN:
    push ebp
    mov ebp, esp
    
    and esp, ~0xf
    
    sub esp, 8
    push open_mode_1
    push filename_1
    
    call fopen
    
    add esp, 16
    
    mov dword[fp], eax
   
    xor ecx, ecx
  .while: 
    push ecx
    push x
    push f_scanf
    push dword[fp]
    
    call fscanf
    
    add esp, 12
    pop ecx
    
    cmp eax, -1
    je .end_while
        
    mov ebx, [x]
    
    mov dword[arr + ecx * 4], ebx
    
    inc ecx
    jmp .while
       
  .end_while:
    
    push ecx
    sub esp, 8
    push dword[fp]
    
    call fclose
    
    add esp, 12
    pop ecx      

    push compare
    push INT_SIZE
    push ecx
    push arr
    
    call qsort
    
    add esp, 4
    pop ecx
    add esp, 8    
    
    push ecx
    sub esp, 4
    push open_mode_2
    push filename_2
    
    call fopen
    
    add esp, 12
    pop ecx
    
    mov dword[fp], eax
    
    mov ebx, ecx
    xor ecx, ecx 
  .for_print:
    push ecx
    push dword[arr + ecx * 4]
    push f_print
    push dword[fp]
    
    call fprintf
    
    add esp, 12
    pop ecx
   
    inc ecx
    
    cmp ecx, ebx
    jl .for_print
    
    sub esp, 12
    push dword[fp]
    
    call fclose
    
    add esp, 16
   
    mov esp, ebp
    pop ebp
    
    xor eax, eax
    ret
    
    
compare:
    push ebp
    mov ebp, esp
    
    push edi
    push esi
    
    mov esi, [ebp + 8]
    mov edi, [ebp + 12]
    mov esi, [esi]
    mov edi, [edi]
    
    cmp esi, edi
    jl .below
    jg .above
    
    mov eax, 0
    jmp .end_compare
    
  .below:
    mov eax, -1
    jmp .end_compare
  .above:
    mov eax, 1  
    
  .end_compare: 
  
    pop esi
    pop edi 
    add esp, 8
    mov esp, ebp
    pop ebp
    
    ret
