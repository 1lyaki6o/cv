%include "io.inc"

extern scanf
extern fprintf
extern malloc
extern free
extern get_stdout

 
section .data

section .bss
    n resd 1
    arr resd 1
section .rodata
    fmt_p db `%d\n`, 0
    fmt_s db `%d`, 0  

    
section .text
GLOBAL CMAIN
CMAIN:
    push ebp
    mov ebp, esp
    
    and esp, ~0xf
    
    sub esp, 8
    push n
    push fmt_s
    
    call scanf
    
    add esp, 16
    
    mov ebx, 4
    mov eax, dword[n]
    mul ebx
    
    sub esp, 12
    push eax
    
    call malloc
    
    add esp, 16
    
    mov dword[arr], eax
    
    xor esi, esi
  .for_scanf:
    cmp esi, dword[n]
    jge .end_scanf
    
    lea eax, [esi * 4]
    add eax, dword[arr]
    sub esp, 8
    push eax
    push fmt_s
    
    call scanf
    
    add esp, 16
    
    inc esi
    jmp .for_scanf
  .end_scanf:
  
    call get_stdout
    mov edi, eax
    
    sub esp, 8
    push fmt_p ; +28
    push edi ; +24
    push 2 ; +20
    push fprintf ; +16
    push dword[n] ; +12
    push dword[arr] ; +8
  
    call apply
    
    add esp, 32 
    
    sub esp, 12
    push dword[arr]
    
    call free
    
    add esp, 16   
  
    mov esp, ebp
    pop ebp
    
    xor eax, eax
    ret
    
apply:
    push ebp
    mov ebp, esp
    sub esp, 12
    push esi
    push edi
    push ebx
    
    mov eax, [ebp + 20]
    inc eax
    mov ebx, 4
    xor edx, edx
    div ebx
    sub ebx, edx
    mov eax, ebx
    mov ebx, 4
    xor edx, edx
    div ebx
    mov edi, edx ; (4-(n+1)%4)%4 
    
    mov ebx, edi
    add ebx, dword[ebp + 20]
    inc ebx
    lea ebx, [ebx * 4] ;((4-(n+1)%4)%4 + (n+1))*4
    lea edi, [edi * 4] ;((4-(n+1)%4)%4)*4   
    
    xor esi, esi
  .for_fprintf:
    cmp esi, dword[ebp + 12]
    jge .end_fprintf
    
    lea eax, [esi * 4]
    add eax, [ebp + 8]
    
    sub esp, edi
    push dword[eax]
    mov ecx, [ebp + 20]
  .for_push: 
    cmp ecx, 0
    jle .end_push
    
    lea eax, [ecx * 4 + 20]
    add eax, ebp
    
    push dword[eax]
    
    dec ecx
    jmp .for_push
  .end_push:
    
    call dword[ebp + 16]
    
    add esp, ebx
    
    inc esi
    jmp .for_fprintf       
         
  .end_fprintf: 
    pop ebx
    pop edi
    pop esi   
     
    mov esp, ebp
    pop ebp
    
    xor eax, eax
    ret   
