%include "io.inc"

mod equ 2011

section .bss
    notation resd 1
    def_numb resd 1
    i_numb resd 1    
section .data

   
section .text
global CMAIN
CMAIN:
    push ebp
    mov ebp, esp
    
    GET_UDEC 4, notation 
    GET_UDEC 4, i_numb
    GET_DEC 4, def_numb
    
    xor edx, edx
    mov eax, [def_numb]
    mov ebx, mod
    idiv ebx
    mov eax, edx
    push eax
    
       
    mov ecx, 1
  .for:
    cmp ecx, dword[i_numb]
    jg .end_for
    
    mov ebx, 1
    mov esi, 0
    
    push eax
       
    call splic_numb
    
    cmp dword[esp], 0
    jne .skip
    imul ebx, dword[notation]
  .skip:
    
    add esp, 4
        
    call splic_numb
        
    xor edx, edx
    mov eax, esi
    mov ebx, mod
    idiv ebx
    mov eax, edx
        
    xchg eax, [esp]
    inc ecx    
    jmp .for
        
  .end_for:  
    mov eax, [esp] 
    add esi, 4   
    PRINT_DEC 4, eax 
    
    mov esp, ebp
    pop ebp
    
    xor eax, eax
    ret
    
    

splic_numb:
    push ebp
    mov ebp, esp
    mov eax, [ebp + 8]

  .while:
    cmp eax, 0
    jle .end    
    
    xor edx, edx
    idiv dword[notation]
    imul edx, ebx
    add esi, edx
    
    imul ebx, [notation]
    
    jmp .while
    
  .end:
    mov esp, ebp
    pop ebp
    
    ret
    
    
