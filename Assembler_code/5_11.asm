%include "io.inc"

extern fopen
extern fclose
extern fscanf
extern fprintf
extern malloc
extern free
 
NODE_SIZE equ 12
 
section .data

section .bss
    fp resd 1
    n resd 1
    m resd 1
    x resd 1
    y resd 1
    head_pointer resd 1
    arr_pnt resd 100000
section .rodata
    filename_1 db `input.txt`, 0
    filename_2 db `output.txt`, 0
    mode_1 db `r`, 0
    mode_2 db `w`, 0
    fmt_p db `%d `, 0
    fmt_s db `%d%d`, 0  

    
section .text
GLOBAL CMAIN
CMAIN:
    push ebp
    mov ebp, esp
    
    and esp, ~0xf
    
    mov ebx, 0 ;head
    
    ;open input.txt
    sub esp, 8
    push mode_1
    push filename_1
    
    call fopen
    
    mov dword[fp], eax
    
    add esp, 16
    
    push m
    push n
    push fmt_s
    push dword[fp]
    
    call fscanf
    
    add esp, 16
    
    mov ecx, dword[n]
  .push_for:
    cmp ecx, 0
    jle .end_push
    
    push ecx
    sub esp, 4
    push ecx
    push ebx
    
    call push_front
    
    add esp, 12
    pop ecx
    
    mov ebx, eax
    
    mov dword[arr_pnt + ecx * 4], ebx
    
    dec ecx
    jmp .push_for
  .end_push:
  
    xor esi, esi
  .mixing:
    cmp esi, dword[m]
    jge .end_mix
    
    push y
    push x
    push fmt_s
    push dword[fp]
    
    call fscanf
    
    add esp, 16
    
    mov eax, dword[x]
    mov edx, dword[y]
    
    mov edi, [arr_pnt + eax * 4]
    mov edi, [edi + 8]
    
    cmp edi, 0
    je .if_1
    
    mov ecx, [arr_pnt + edx * 4]
    mov ecx, [ecx + 4]
    mov dword[edi + 4], ecx
    
    cmp ecx, 0
    je .if_2
    mov edi, [arr_pnt + eax * 4]
    mov edi, [edi + 8]
    mov dword[ecx + 8], edi    
  .if_2:
    mov ecx, [arr_pnt + edx * 4]
    mov dword[ecx + 4], ebx
    mov dword[ebx + 8], ecx  
    mov edi, [arr_pnt + eax * 4]
    mov dword[edi + 8], 0
    mov ebx, edi  
  .if_1: 
    inc esi
    jmp .mixing    
  .end_mix: 
    
    sub esp, 12
    push dword[fp]
    
    call fclose
    
    add esp, 16
    ;close input.txt
    
    ;open output.txt
    sub esp, 8
    push mode_2
    push filename_2
    
    call fopen
    
    mov dword[fp], eax
    
    add esp, 16
     
    sub esp, 8
    push dword[fp]
    push ebx
    
    call print_list
    
    add esp, 16
    
    sub esp, 12
    push dword[fp]
    
    call fclose
    
    add esp, 16  
    ;close output.txt
    
    mov dword[head_pointer], ebx
    
    sub esp, 12
    push dword head_pointer
    
    call delete_list
    
    add esp, 16
        
    
    mov esp, ebp
    pop ebp
    
    xor eax, eax
    ret
    
    
    
push_front:
    push ebp
    mov ebp, esp
    sub esp, 12
    push edi
    push esi
    push ebx
    
    mov esi, dword[ebp + 8]
    mov edi, dword[ebp + 12]
    
    sub esp, 12
    push NODE_SIZE
    
    call malloc
    
    add esp, 16
    
    mov dword[eax], edi
    mov dword[eax + 4], esi
    mov dword[eax + 8], 0
    
    cmp dword[ebp + 8], 0
    je .return_new_node
    mov edi, dword[ebp + 8]
    mov dword[edi + 8], eax    
    
  .return_new_node:
    pop ebx
    pop esi
    pop edi
    mov esp, ebp
    pop ebp
    
    ret
    
print_list:
    push ebp
    mov ebp, esp
    sub esp, 12
    push esi
    push edi
    push ebx
    
    mov esi, [ebp + 8]
    
  .prt_while:
    cmp esi, 0
    je .end_prt_while
    
    sub esp, 4
    push dword[esi]
    push fmt_p
    push dword[ebp + 12] ;file pointer
    
    call fprintf
    
    add esp, 16
    
    mov esi, [esi + 4]
    
    jmp .prt_while
    
  .end_prt_while: 
    pop ebx 
    pop edi
    pop esi
    
    mov esp, ebp
    pop ebp
    
    xor eax, eax
    
    ret
    
    
delete_list:
    push ebp
    mov ebp, esp
    sub esp, 12
    push edi
    push esi
    push ebx
    
    mov esi, [ebp + 8]
    mov esi, [esi] ;cur
    
  .del_while:
    cmp esi, 0
    je .end_del_while
    mov edi, [esi + 4]
    
    sub esp, 12
    push esi
    
    call free
    
    add esp, 16
    
    mov esi, edi
    
    jmp .del_while
    
    
  .end_del_while:    
    pop ebx  
    pop esi
    pop edi
    
    mov esp, ebp
    pop ebp
    
    xor eax, eax
    
    ret