%include "io.inc"

section .bss
    sum_x resd 1
    sum_y resd 1

    
section .data

   
section .text
global CMAIN
CMAIN:
    push ebp
    mov ebp, esp
    GET_UDEC 4, ecx
    GET_UDEC 4, sum_x
    GET_UDEC 4, sum_y
    dec ecx
    
    cmp ecx, 0
    jle .skip
  .for:
    GET_UDEC 4, eax
    GET_UDEC 4, ebx
    mov edi, [sum_x]
    imul edi, ebx
    mov dword[sum_x], edi
    imul eax, dword[sum_y]
    mov edi, [sum_y] 
    imul edi, ebx
    mov dword[sum_y], edi
    add dword[sum_x], eax 
    dec ecx
  .skip:
    push dword[sum_y]
    push dword[sum_x]
    
    call gcd
    
    add esp, 8
    
    mov ebx, eax
    xor edx, edx
    mov eax, [sum_y]
    div ebx
    mov dword[sum_y], eax
    xor edx, edx
    mov eax, [sum_x]
    div ebx
    mov dword[sum_x], eax
    
    cmp ecx, 0
    jg .for
    
    
    PRINT_UDEC 4, sum_x
    PRINT_STRING ` `
    PRINT_UDEC 4, sum_y
    
    
    
    mov esp, ebp
    pop ebp
    
    xor eax, eax
    ret
    
    
gcd:
    push ebp
    mov ebp, esp
    sub esp, 8
    mov eax, [ebp + 8]
    mov ebx, [ebp + 12]
    test ebx, ebx
    jnz .recur
    jmp .end_gcd
  .recur:
    xor edx, edx
    div ebx
    mov dword[esp], ebx
    mov dword[esp + 4], edx
    call gcd
  .end_gcd:
    mov esp, ebp
    pop ebp
    
    ret
