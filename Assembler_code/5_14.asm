%include "io.inc"

extern fopen
extern fclose
extern fscanf
extern fprintf
extern malloc
extern free
 
NODE_SIZE equ 8 
 
section .data

section .bss
    fp resd 1
    swap_flag resd 1
    x resd 1
    head_pointer resd 1
section .rodata
    filename_1 db `input.txt`, 0
    filename_2 db `output.txt`, 0
    mode_1 db `r`, 0
    mode_2 db `w`, 0
    fmt_p db `%d `, 0
    fmt_s db `%d`, 0  

    
section .text
GLOBAL CMAIN
CMAIN:
    push ebp
    mov ebp, esp
    
    and esp, ~0xf
    
    mov ebx, 0 ;head
    
    ;open input.txt
    sub esp, 8
    push mode_1
    push filename_1
    
    call fopen
    
    mov dword[fp], eax
    
    add esp, 16
    
  .while: 
    sub esp, 4
    push dword x
    push fmt_s
    push dword[fp]
    
    call fscanf
    
    add esp, 16
    
    cmp eax, -1
    je .end_while
        
    sub esp, 8
    push dword[x]
    push ebx
    
    call push_front
    
    add esp, 16
    
    mov ebx, eax
    
    jmp .while
       
  .end_while:
    
    sub esp, 12
    push ebx
    
    call bubble_sort
    
    add esp, 16  
    
    sub esp, 12
    push dword[fp]
    
    call fclose
    
    add esp, 16
    ;close input.txt
    
    ;open output.txt
    sub esp, 8
    push mode_2
    push filename_2
    
    call fopen
    
    mov dword[fp], eax
    
    add esp, 16
     
    sub esp, 8
    push dword[fp]
    push ebx
    
    call print_list
    
    add esp, 16
    
    sub esp, 12
    push dword[fp]
    
    call fclose
    
    add esp, 16  
    ;close output.txt
    
    mov dword[head_pointer], ebx
    
    sub esp, 12
    push head_pointer
    
    call delete_list
    
    add esp, 16
    
    mov esp, ebp
    pop ebp
    
    xor eax, eax
    ret
    
    
push_front:
    push ebp
    mov ebp, esp
    sub esp, 12
    push edi
    push esi
    push ebx
    
    mov esi, dword[ebp + 8]
    mov edi, dword[ebp + 12]
    
    sub esp, 12
    push NODE_SIZE
    
    call malloc
    
    add esp, 16
    
    mov dword[eax], edi
    mov dword[eax + 4], esi
    
    pop ebx
    pop esi
    pop edi
    
    mov esp, ebp
    pop ebp
    
    ret
    
    
    
delete_list:
    push ebp
    mov ebp, esp
    sub esp, 12
    push edi
    push esi
    push ebx
    
    mov esi, [ebp + 8]
    mov esi, [esi] ;cur
    
  .del_while:
    cmp esi, 0
    je .end_del_while
    mov edi, [esi + 4]
    
    sub esp, 12
    push esi
    
    call free
    
    add esp, 16
    
    mov esi, edi
    
    jmp .del_while
    
    
  .end_del_while:    
    pop ebx  
    pop esi
    pop edi
    
    mov esp, ebp
    pop ebp
    
    xor eax, eax
    
    ret
    
    
print_list:
    push ebp
    mov ebp, esp
    push esi
    push ebx
    
    mov esi, [ebp + 8]
    
  .prt_while:
    cmp esi, 0
    je .end_prt_while
    
    sub esp, 4
    push dword[esi]
    push fmt_p
    push dword[ebp + 12] ;file pointer
    
    call fprintf
    
    add esp, 16
    
    mov esi, [esi + 4]
    
    jmp .prt_while
    
  .end_prt_while: 
    pop ebx 
    pop esi
    mov esp, ebp
    pop ebp
    
    xor eax, eax
    
    ret
    
    
bubble_sort:
    push ebp
    mov ebp, esp
    push edi
    push esi
    push ebx
    
    mov edi, 0
    
    cmp dword[ebp + 8], 0
    je .end_sort
    
  .do_while:
    mov dword[swap_flag], 0
    mov esi, [ebp + 8]
      .while_b:
        cmp dword[esi + 4], edi
        je .end_while_b
        mov ebx, [esi + 4]
        mov ebx, [ebx]
        cmp dword[esi], ebx
        jle .next_step 
        
        sub esp, 8
        push esi
        push dword[esi + 4]
        
        call swap
        
        add esp, 16
        
        mov dword[swap_flag], 1
      .next_step:
        mov esi, [esi + 4]
        
        jmp .while_b
      .end_while_b:
    mov edi, esi
    
    cmp dword[swap_flag], 1
    je .do_while
    
  .end_sort:  
    pop ebx
    pop esi
    pop edi
    mov esp, ebp
    pop ebp
    
    xor eax, eax
    
    ret
    
 
swap:
    push ebp
    mov ebp, esp
    push esi
    push edi
    push ebx
    
    mov esi, dword[ebp + 8]
    mov edi, dword[ebp + 12]
    
    push dword[esi]
    mov ebx, [edi]
    mov dword[esi], ebx
    pop ebx
    mov dword[edi], ebx
    
    pop ebx
    pop edi
    pop esi
    mov esp, ebp
    pop ebp
    
    xor eax, eax
    
    ret