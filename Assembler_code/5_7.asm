%include "io.inc"

extern scanf
extern printf
extern strcmp
 
SHIFT_STR equ 11 
 
section .data

section .bss
    n resd 1
    string resb 5501
    arr_flags resd 500
    
section .rodata
    fmt_p db `%d`, 0
    fmt_s1 db `%10s`, 0 
    fmt_s2 db `%d`, 0

    
section .text
GLOBAL CMAIN
CMAIN:
    push ebp
    mov ebp, esp
    
    and esp, ~0xf
    
    sub esp, 8
    push n
    push fmt_s2
    
    call scanf
    
    add esp, 16
    
    mov esi, [n]
    mov edi, string
  .scanf_for:
    cmp esi, 0
    jle .end_scanf
    
    sub esp, 8
    push edi
    push fmt_s1
    
    call scanf
    
    add esp, 16
    
    dec esi
    add edi, SHIFT_STR
    
    jmp .scanf_for
  .end_scanf:
 
    mov edi, string ;i
    mov esi, string ;j
    xor ecx, ecx ;i
    xor ebx, ebx ;j
  .for_i:
    cmp ecx, dword[n]
    jge .end_i 
    
    mov ebx, ecx
    inc ebx
    mov esi, edi
      .for_j:
        cmp ebx, dword[n]
        jge .end_j
        
        add esi, SHIFT_STR

        push ecx
        sub esp, 4
        push edi
        push esi
        
        call strcmp
        
        add esp, 12
        pop ecx
        
        cmp eax, 0 ;strcmp return
        jne .skip
        
        mov dword[arr_flags + ebx * 4], 1        
      .skip:
    
        inc ebx
        jmp .for_j
      .end_j:
    
    inc ecx
    add edi, SHIFT_STR
    jmp .for_i
  .end_i:
    
    xor eax, eax
    xor ecx, ecx
  .cnt_unic:
    cmp ecx, dword[n]
    jge .print
    
    cmp dword[arr_flags + ecx * 4], 1
    je .l1
    inc eax 
  .l1:   
    inc ecx
    jmp .cnt_unic  
  .print: 
    sub esp, 8
    push eax
    push fmt_p
    
    call printf
    
    add esp, 16
    
    mov esp, ebp
    pop ebp
    
    xor eax, eax
    ret
    
    
