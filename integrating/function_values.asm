global f_1
global f_2
global f_3

; 1 + 4/(x^2 + 1)

f_1:
    push ebp
    mov ebp, esp
    
    push ebx
    push edi 
    push esi
    
    fld1
    fld1
    fld1
    fld1
    faddp
    faddp
    faddp

    fld qword[ebp + 8]
    fld qword[ebp + 8]
    fmulp
    fld1
    faddp
    fdivp
    fld1
    faddp    
    
    pop esi
    pop edi
    pop ebx
    
    mov esp, ebp
    pop ebp
    
    ret 
    
; x^3    
    
f_2:
    push ebp
    mov ebp, esp
    
    push ebx
    push edi 
    push esi
    
    fld qword[ebp + 8]
    fld qword[ebp + 8]
    fld qword[ebp + 8]
    fmulp
    fmulp    
    
    pop esi
    pop edi
    pop ebx
    
    mov esp, ebp
    pop ebp
    
    ret 
    
; 2^(-x)    
    
f_3:
    push ebp
    mov ebp, esp
    
    push ebx
    push edi 
    push esi
    
    fld1
    fld qword[ebp + 8]
    fld qword[ebp + 8]
    frndint
    fsub st1, st0
    fld1
    fscale
    fld st2
    f2xm1
    fstp st2
    fstp st2
    fld1
    faddp
    fmulp
    fdivp
    
    pop esi
    pop edi
    pop ebx
    
    mov esp, ebp
    pop ebp
    
    ret 
