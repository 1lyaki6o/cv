#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

typedef double(*func)(double x);
typedef double dbl;

extern dbl f_1(dbl x); // 1 + 4/(x^2 + 1)
extern dbl f_2(dbl x); // x^3
extern dbl f_3(dbl x); // 2^(-x)

static int cnt_iterations;

// Finding the abscissa of the intersection point of function graphs using the Chord Method
dbl root(func f, func g, dbl a, dbl b, dbl eps1){
	cnt_iterations = 0;
	dbl c_cure = (a * (f(b) - g(b)) - b * (f(a) - g(a)))/((f(b) - g(b)) - (f(a) - g(a)));

	if ((f(a) - g(a)) * (f(c_cure) - g(c_cure)) < 0){
		b = c_cure;
	}else if ((f(b) - g(b)) * (f(c_cure) - g(c_cure)) < 0){
		a = c_cure;
	}

	cnt_iterations++;

	dbl c_next = c_cure;

	do{
		c_cure = c_next;
		c_next = (a * (f(b) - g(b)) - b * (f(a) - g(a)))/((f(b) - g(b)) - (f(a) - g(a)));

		if ((f(a) - g(a)) * (f(c_next) - g(c_next)) < 0){
			b = c_next;
		}else if ((f(b) - g(b)) * (f(c_next) - g(c_next)) < 0){
			a = c_next;
		}

		cnt_iterations++;
	}while (fabs(c_next - c_cure) > eps1);

	return c_next;
}

//Finding a definite integral using the Rectangular method
dbl integral(func f, dbl a, dbl b, dbl esp2){
	int n0 = 100;
	dbl h = (b - a) / n0;
	dbl I_n = 0;
	dbl I_2n = 0;

	for (int i = 0; i < n0; i++){
		I_n += f(a + (i + 0.5) * h);
	}

	I_n *= h;

	for (int i = 0; i < 2*n0; i++){
		I_2n += f(a + (i + 0.5) * h/2);
	}

	I_2n *= h/2;

	while (fabs(I_2n - I_n)/3 > esp2){
		n0 *= 2;
		h = (b - a) / n0;
		I_n = 0;
		I_2n = 0;

		for (int i = 0; i < n0; i++){
			I_n += f(a + (i + 0.5) * h);
		}

		I_n *= h;

		for (int i = 0; i < 2*n0; i++){
			I_2n += f(a + (i + 0.5) * h/2);
		}

		I_2n *= h/2;
	}

	return I_2n;
}

void testing_root(func f_1, func f_2, func f_3, const char* func_1, const char* func_2, const char* left, const char* right, const char* esp_1){
	func f, g;
	dbl a = atof(left);
	dbl b = atof(right);
	dbl esp = atof(esp_1);

	if (!strcmp(func_1, "f_1")){
		f = f_1;
	}else if (!strcmp(func_1, "f_2")){
		f = f_2;
	}else{
		f = f_3;
	}
	
	if (!strcmp(func_2, "f_1")){
		g = f_1;
	}else if (!strcmp(func_2, "f_2")){
		g = f_2;
	}else{
		g = f_3;
	}

	dbl res = root(f, g, a, b, esp);

	if (b <= a || isnan(res)){ //Checking for errors 
		printf("The data is not entered correctly\n\n");	
	}else if (res < a || res > b){
		printf("The functions %s and %s do not intersect on the interval [%lf, %lf]\n\n", func_1, func_2, a, b);
	}else{
		printf("The selected functions %s and %s intersect at the point == (%lf, %lf)\n\n", func_1, func_2, res, f(res));	
	}		
}


void testing_integral(func f_1, func f_2, func f_3, const char* func_1, const char* left, const char* right, const char* esp_2){
	func f;
	dbl a = atof(left);
	dbl b = atof(right);
	dbl esp = atof(esp_2);

	if (!strcmp(func_1, "f_1")){
		f = f_1;
	}else if (!strcmp(func_1, "f_2")){
		f = f_2;
	}else{
		f = f_3;
	}

	dbl res = integral(f, a, b, esp);

	if (b <= a || isnan(res)){ //Checking for errors 
		printf("\nThe data is not entered correctly\n\n");
	}else{
		printf("\nThe area of the figure under the graph %s: S == %lf\n\n", func_1, res);	
	}	
}

int main(int argc, char const *argv[]){
	printf("To see a list of available commands, add the -help key\n\n");

	dbl esp1 = 0.0001, esp2 = 0.0001;
	dbl tmp;

	for (int i = 0; i < argc; i++){
		if(!strcmp(argv[i], "-help")){
			printf("List of commands:\n\n");
			printf("\t[-intersec_points] -- to see the intersection points of the graphs\n\n");
			printf("\t[-iterations] -- to see the number of iterations it took to find the intersection point of the graphs\n\n");
			printf("\t[-test_root] -- to test the root() function or see how it works with prepared tests\n\n");
			printf("\tParameters: <func_1> <func_2> <a> <b> <esp>\n");
			printf("\t\t<func_1> - first function f_1/f_2/f_3\n\t\t<func_2> - second function f_1/f_2/f_3\n\t\t<a> - left border of the segment\n");
			printf("\t\t<b> - right border of the segment\n\t\t<esp> - accuracy of calculations\n\n");
			printf("\t[-test_integral] -- to test the interval() function or see how it works with prepared tests\n\n");
			printf("\tParameters: <func> <a> <b> <esp>\n");
			printf("\t\t<func> - function f_1/f_2/f_3\n\t\t<a> - left border of the segment\n");
			printf("\t\t<b> - right border of the segment\n\t\t<esp> - accuracy of calculations\n\n");
			printf("\t[-res] -- to see the area of the figure bounded by the given curves: f_1, f_2, f_3\n\n");
		}

		if (!strcmp(argv[i], "-intersec_points")){
			printf("Points of intersection of defined functions:\n\n");
			tmp = root(f_1, f_2, 0.5, 2, esp1);
			printf("f_1 and f_2 --> (%lf, %lf)\n\n", tmp, f_1(tmp));
			tmp = root(f_2, f_3, 0.5, 2, esp1);
			printf("f_2 and f_3 --> (%lf, %lf)\n\n", tmp, f_2(tmp));
			tmp = root(f_1, f_3, -1.5, -0.8, esp1);
			printf("f_1 and f_3 --> (%lf, %lf)\n\n", tmp, f_1(tmp));
		}

		if(!strcmp(argv[i], "-iterations")){
			printf("The number of iterations required to find the intersection point of the graphs:\n\n");
			root(f_1, f_2, 0.5, 2, esp1); // Call the root function so that the counter "cnt_iterations" will change for the set functions
			printf("\tfunctions f_1 abd f_2:  iterations == %d\n\n", cnt_iterations);
			root(f_2, f_3, 0.5, 2, esp1);
			printf("\tfunctions f_2 abd f_3:  iterations == %d\n\n", cnt_iterations);
			root(f_1, f_3, -1.5, -0.8, esp1);
			printf("\tfunctions f_1 abd f_3:  iterations == %d\n\n", cnt_iterations);
		}

		if(!strcmp(argv[i], "-test_root")){
			testing_root(f_1, f_2, f_3, argv[i + 1], argv[i + 2], argv[i + 3], argv[i + 4], argv[i + 5]);
			i += 5;
		}

		if(!strcmp(argv[i], "-test_integral")){
			testing_integral(f_1, f_2, f_3, argv[i + 1], argv[i + 2], argv[i + 3], argv[i + 4]);
			i += 4;
		}

		//Calculation of the final area under the graph
		if(!strcmp(argv[i], "-res")){
			dbl tmp1 = integral(f_1, -1.308, 1.334, esp2) - integral(f_3, -1.308, 0.826, esp2) - integral(f_2, 0.826, 1.344, esp2);
			printf("Area of the figure bounded by the given curves: f_1, f_2, f_3 --> S == %lf\n\n", tmp1);
		}
	}

	return 0;
}