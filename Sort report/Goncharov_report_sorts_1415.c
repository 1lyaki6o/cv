// 1/4/1/5

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define RAND_NEG (1 << 29)

//Подсчет количества сравнений и обменов элементов массива.
int compare = 0;
int exchange = 0;

//Функция сравнения двух int чисел по модулю.
int cmp(const void* x, const void* y){
    compare++;

    const int* a = x;
    const int* b = y;

    return abs(*b) - abs(*a);
}

//Функция обмена значений двух элементов массива.
void swap (int* x, int* y){
    int tmp = *x;
    *x = *y;
    *y = tmp;

    exchange++;
}

//Функция сортировки методом "пузырька".
void BubbleSort (int* arr, int n){
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n - i - 1; j++){
            if (cmp(&arr[j], &arr[j + 1]) > 0){
                swap (&arr[j], &arr[j + 1]);
            }
        }
    }
}

//ФУНКЦИИ ПИРАМИДАЛЬНОЙ СОРТИРОВКИ:

//Функция heapify - поддержание свойства неубывающей пирамиды (значение узла не менее значения родительского по отношению к нему).
void heapify (int* arr, int n, int i) { // i - индекс элемента массива, который необходимо проверить на удовлетворение свойству пирамиды. 
    int l = i * 2 + 1; // индекс левого потомока
    int r = i * 2 + 2; // индекс правого потомока
    int least;

    if (l < n && cmp(&arr[l], &arr[i]) > 0){
        least = l;
    }else {
        least = i;
    }

    if (r < n && cmp(&arr[r], &arr[least]) > 0){
        least = r;
    }

    if (least != i){
        swap(&arr[i], &arr[least]);
        heapify(arr, n, least);
    }
}

//Функция build_heap - построение пирамиды
void build_heap(int* arr, int n){
    for(int i = n / 2 - 1; i >= 0; i--){
        heapify(arr, n, i);
    }
}

//Функция heapsort - пирамидальная сортировка
void heapsort (int* arr, int n){
    build_heap(arr, n);

    for (int i = n - 1; i > 0; i--){
        swap(&arr[0], &arr[i]);
        heapify(arr, i, 0);
    }
}

//Функция поиска среднего значения элементов массива.
double arr_average(int* arr, int n){
    int sum = 0;

    for (int i = 0; i < n; i++) {
        sum += arr[i];
    }

    double average_sum = ((double)sum / (double)n);

    return average_sum;
}

//Функция создание массива (flag_sort == 0 -> эл-ты уже упорядочены, flag_sort == 1 -> эл-ты упорядочены в обратном поряжке, else -> расстановка элементов случайна).
void create_arr(int* arr, int n, int flag_sort){
    if (flag_sort == 1){
        for (int i = 0; i < n; i++){
            arr[i] = i;
        }
    }else if (flag_sort == 0){
        for (int i = 0; i < n; i++){
            arr[i] = n - i;
        }
    }else {
        for (int i = 0; i < n; i++){
            arr[i] = rand() * rand() - RAND_NEG;
        }
    }
}

//Выбор сортировки и проверка корректности сортировки (mode == 0 -> heapsort, mode == 1 -> BubbleSort).
void select_sort(int* arr, int n, int mode){
    int* x = (int*)malloc(sizeof(int) * n);

    for (int i = 0; i < n; i++){ //Создание копии массива arr
        x[i] = arr[i];
    }

    if (mode == 0){
        heapsort(arr, n);
    }else {
        BubbleSort(arr, n);
    }

    int tmp = compare;

    //Проверка корректности сортировки с помощью qsort
    qsort(x, n, sizeof(int), cmp);

    for(int i = 0; i < n; i++){ 
        if (x[i] != arr[i]){
            printf("Not correct sorting");
            abort();
        }
    }

    compare = tmp;

    free(x);
}

int main(void) {
    srand(time(NULL));

    int *arr_exch = (int*)malloc(4 * sizeof(int)); //массив кол-ва обменов.
    int *arr_comp = (int*)malloc(4 * sizeof(int)); //массив кол-ва сравнений.

    // СОРТИРОВКА HEAPSORT И ОБРАБОТКА РЕЗУЛЬТАТОВ.
    printf("\t\tHEAPSORT\n\n");

    int n = 10; //Кол-во элементов массива.

    int *arr_h = (int*)malloc(n * sizeof(int)); //массив для сортировки heapsort.

    for (int i = 0; i < 4; i++){
        for (int j = 0; j < 4; j++){
            create_arr(arr_h, n, j);
            select_sort(arr_h, n, 0);

            arr_exch[j] = exchange;
            arr_comp[j] = compare;

            exchange = 0;
            compare = 0;
        }

        double average_sum_exch = arr_average(arr_exch, 4);
        double average_sum_comp = arr_average(arr_comp, 4);

        //Вывод данных.
        printf("n:%d\n", n);
        printf("exchange  ");

        for(int j = 1; j <= 4; j++){
            printf("%d:%d\t", j, arr_exch[j - 1]);
        }

        printf("avg:%lf\n\n", average_sum_exch);

        printf("compare  ");

        for(int j = 1; j <= 4; j++){
            printf("%d:%d\t", j, arr_comp[j - 1]);
        }

        printf("avg:%lf\n\n\n", average_sum_comp);

        //Расширение массива arr_h.
        n *= 10;

        arr_h = realloc(arr_h, n * sizeof(int));
    }

    free(arr_h);

    // СОРТИРОВКА BUBBLESORT И ОБРАБОТКА РЕЗУЛЬТАТОВ.
    printf("\t\tBUBBLESORT\n\n");

    n = 10; //Кол-во элементов массива.

    int *arr_b = (int*)malloc(n * sizeof(int)); //массив для сортировки bubblesort.

    for (int i = 0; i < 4; i++){
        for (int j = 0; j < 4; j++){
            create_arr(arr_b, n, j);
            select_sort(arr_b, n, 1);

            arr_exch[j] = exchange;
            arr_comp[j] = compare;

            exchange = 0;
            compare = 0;
        }

        double average_sum_exch = arr_average(arr_exch, 4);
        double average_sum_comp = arr_average(arr_comp, 4);

        //Вывод данных.
        printf("n:%d\n", n);
        printf("exchange  ");

        for(int j = 1; j <= 4; j++){
            printf("%d:%d\t", j, arr_exch[j - 1]);
        }

        printf("avg:%lf\n\n", average_sum_exch);

        printf("compare  ");

        for(int j = 1; j <= 4; j++){
            printf("%d:%d\t", j, arr_comp[j - 1]);
        }

        printf("avg:%lf\n\n\n", average_sum_comp);

        //Расширение массива arr_b.
        n *= 10;

        arr_b = realloc(arr_b, n * sizeof(int));
    }

    free(arr_b);
    free(arr_comp);
    free(arr_exch);

    return 0;
}
